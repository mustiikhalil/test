//: Playground - noun: a place where people can play

import UIKit


// example:

// Network_Protocols.swift

protocol Networking{
    var BASE_URL: String {get}
    
    func authAccount(user: String, pass: String) -> Bool
    
    func getTweets(from Url: String, params: [String: String]) -> [String: String]
}


// Networking_Class.swift

class Network: Networking{
    var BASE_URL: String = "sometypeOfURL"
    
    func authAccount(user: String, pass: String) -> Bool {
        // does networking things
        
        // return value example
        return true
    }
    
    func getTweets(from Url: String, params: [String : String]) -> [String : String] {
        
        // does some fetching of data
        
        // return example value
        return ["": ""]
    }
}

// In the Constants.swift
let network = Network()



// ViewController.swift


class ViewController {
    
    
    var tweets: [String: String] = [:]
    // all the methods for the UIViewController
    
    func logInButtonPressed(){
        if network.authAccount(user: "userField", pass: "userPass") {
            print("accessed")
        }
        else{
            print("denied")
        }
    }
    
    // reload button pressed
    func reloadData(){
        tweets = network.getTweets(from: "SomeURL", params: ["":""])
    }
    
    // add the new data to the UITable
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        
        return UITableViewCell()
    }
    
    
}

